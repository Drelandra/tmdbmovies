//
//  GenresMoviesRouter.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SwiftBaseArchitecture

protocol GenresMoviesRouting: Routing {
    func routeToDiscoverMovies(_ genre: Genre)
}

final class GenresMoviesRouter: GenresMoviesRouting {
    
    weak var baseViewController: UIViewController?
    
    let moviesFactory: MoviesFactory = MoviesViewFactory()
    
    required init(baseViewController: UIViewController) {
        self.baseViewController = baseViewController
    }
    
    func routeToDiscoverMovies(_ genre: Genre) {
        let viewController = moviesFactory.makeDiscoverMoviesViewController(genre)
        baseViewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}
