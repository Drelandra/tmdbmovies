//
//  DiscoverMoviesRouter.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SwiftBaseArchitecture

protocol DiscoverMoviesRouting: Routing {
    func routeToDetailMovie(_ movie: DiscoverMovie)
}

final class DiscoverMoviesRouter: DiscoverMoviesRouting {
    
    weak var baseViewController: UIViewController?
    
    let moviesFactory: MoviesFactory = MoviesViewFactory()
    
    required init(baseViewController: UIViewController) {
        self.baseViewController = baseViewController
    }
    
    func routeToDetailMovie(_ movie: DiscoverMovie) {
        let viewController = moviesFactory.makeDetailMovieViewController(movie)
        baseViewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}
