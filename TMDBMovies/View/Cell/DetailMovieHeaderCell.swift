//
//  DetailMovieHeaderCell.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SDWebImage

final class DetailMovieHeaderCell: UITableViewCell {
    
    lazy var trailerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton()
        button.alpha = 0.5
        button.contentMode = .scaleAspectFill
        button.setBackgroundImage(UIImage(named: "play_button"), for: .normal)
        button.whenTapped(run: methodOf(self, DetailMovieHeaderCell.playTapped(_:)))
        return button
    }()
    
    lazy var descriptionTitleLabel: UILabel = {
        let label = UILabel()
        label.text = .localized(detailMovie: .descriptionTitle)
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 1
        return label
    }()
    
    lazy var descriptionSubtitleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .regular)
        label.textAlignment = .justified
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    var playTapped: VoidClosure?
    var navBarHeight: CGFloat = 0 {
        didSet {
            guard oldValue < navBarHeight else { return }
            setupConstraints()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }
    
    func setupConstraints() {
        contentView.addSubview(trailerImageView)
        trailerImageView.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(navBarHeight)
            make.horizontalEdges.equalToSuperview()
            make.height.equalTo(250)
        }
        
        trailerImageView.addSubview(playButton)
        playButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(75)
        }
        
        contentView.addSubview(descriptionTitleLabel)
        descriptionTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(trailerImageView.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        contentView.addSubview(descriptionSubtitleLabel)
        descriptionSubtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(descriptionTitleLabel.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-8)
        }
    }
    
    func configure(with model: DetailMovieSection) {
        trailerImageView.sd_setImage(
            with: model.youtubeThumbnailURL,
            placeholderImage: UIImage(named: "background_placeholder")
        )
        descriptionSubtitleLabel.text = model.info
    }
    
    private func playTapped(_ sender: Any) {
        playTapped?()
    }
}
