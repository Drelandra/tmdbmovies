//
//  DiscoverMoviesViewController.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SwiftBaseArchitecture

protocol DiscoverMoviesViewProtocol: AnyObject {
    func displayDiscoverMovies(_ model: [DiscoverMovie])
}

final class DiscoverMoviesViewController: TitledTableViewController<DiscoverMovie>, BindableView {

    typealias Presenter = DiscoverMoviesPresenterProtocol
    var presenter: Presenter?
    
    var selectedGenre: Genre?
    private var discoverMovies: [DiscoverMovie] = [] {
        didSet {
            if discoverMovies.isEmpty {
                tableView.setEmptyView(
                    title: .localized(emptyTable: .errorTitle),
                    message: .localized(emptyTable: .errorSubtitle),
                    buttonTitle: .localized(emptyTable: .errorButtonTitle)
                ) { [weak self] in
                    self?.fetchDiscoverMovies()
                }
            } else {
                tableView.removeEmptyView()
            }
            refreshSnapShot()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = .localized(discoverMovie: .title)
        fetchDiscoverMovies()
    }
    
    func refreshSnapShot(animated: Bool = true) {
        var snapShot = NSDiffableDataSourceSnapshot<DiffableSectionType, DiscoverMovie>()
        snapShot.appendSections([.main])
        snapShot.appendItems(discoverMovies)
        dataSource.apply(snapShot, animatingDifferences: animated)
    }
    
    func fetchDiscoverMovies() {
        presenter?.fetchDiscoverMovies(selectedGenre ?? Genre(id: 28, name: "Action"))
    }
}

//MARK: - DiscoverMoviesViewProtocol

extension DiscoverMoviesViewController: DiscoverMoviesViewProtocol {
    func displayDiscoverMovies(_ model: [DiscoverMovie]) {
        self.discoverMovies = model
    }
}

//MARK: - UITableViewDelegate

extension DiscoverMoviesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedMovie = discoverMovies[safe: indexPath.row] else { return }
        presenter?.routeToDetailMovie(selectedMovie)
    }
}

//MARK: - UITableViewDataSourcePrefetching

extension DiscoverMoviesViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last,
           lastVisibleIndexPath.row >= discoverMovies.count - 5 {
            fetchDiscoverMovies()
        }
    }
}
