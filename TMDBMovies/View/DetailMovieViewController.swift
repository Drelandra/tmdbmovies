//
//  DetailMovieViewController.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SwiftBaseArchitecture
import YouTubePlayerKit

protocol DetailMovieViewProtocol: AnyObject {
    func displayDetailMovie(_ model: DetailMovieSection)
}

final class DetailMovieViewController: UITableViewController, BindableView {
    
    enum SectionType: Int, CaseIterable, Hashable {
        case header
        case userReviews
    }
    
    typealias Presenter = DetailMoviePresenterProtocol
    var presenter: Presenter?
    var selectedMovie: DiscoverMovie?
    var detailMovieSection: DetailMovieSection? {
        didSet {
            if detailMovieSection == nil {
                tableView.setEmptyView(
                    title: .localized(emptyTable: .errorTitle),
                    message: .localized(emptyTable: .errorSubtitle),
                    buttonTitle: .localized(emptyTable: .errorButtonTitle)
                ) { [weak self] in
                    self?.fetchDetailMovies()
                }
            } else {
                tableView.removeEmptyView()
            }
            
            refreshSnapShot()
        }
    }
    
    lazy var dataSource = UITableViewDiffableDataSource<SectionType, AnyHashable>(
        tableView: tableView
    ) { [weak self] tableView, indexPath, item in
        return self?.setupDataSource(tableView, indexPath, item)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        fetchDetailMovies()
    }
    
    func setupUI() {
        title = selectedMovie?.title ?? ""
        
        tableView.dataSource = dataSource
        tableView.prefetchDataSource = self
        tableView.registerEmptyCell()
        tableView.register(DetailMovieHeaderCell.self)
        tableView.register(UserReviewsCell.self)
        tableView.registerHeaderFooter(UserReviewsHeaderView.self)
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 24, right: 0)
        
        tableView.setEmptyView(title: .localized(emptyTable: .loadingTitle), titleOnly: true)
    }
    
    func refreshSnapShot(animated: Bool = true) {
        var snapShot = NSDiffableDataSourceSnapshot<SectionType, AnyHashable>()
        snapShot.appendSections(SectionType.allCases)
        if let detailMovieSection = detailMovieSection {
            snapShot.appendItems([detailMovieSection], toSection: .header)
            if detailMovieSection.userReviews.isNotEmpty {
                snapShot.appendItems(detailMovieSection.userReviews, toSection: .userReviews)
            }
        }
        dataSource.apply(snapShot, animatingDifferences: animated)
    }
    
    func fetchDetailMovies() {
        guard let selectedMovie = selectedMovie else { return }
        presenter?.loadDetailMovie(selectedMovie)
    }
}

//MARK: - DetailMovieViewProtocol

extension DetailMovieViewController: DetailMovieViewProtocol {
    func displayDetailMovie(_ model: DetailMovieSection) {
        self.detailMovieSection = model
    }
}

//MARK: - Data Source Helpers

extension DetailMovieViewController {
    func setupDataSource(
        _ tableView: UITableView,
        _ indexPath: IndexPath,
        _ item: AnyHashable
    ) -> UITableViewCell {
        switch item {
        case let item as DetailMovieSection:
            return dequeueHeaderCell(tableView: tableView, indexPath: indexPath, item: item)
        default:
            guard item is UserReview else {
                return tableView.dequeueVoidCell(for: indexPath) ?? UITableViewCell()
            }
            return dequeueUserReviewsCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func dequeueHeaderCell(
        tableView: UITableView, 
        indexPath: IndexPath,
        item: DetailMovieSection
    ) -> UITableViewCell {
        tableView.setupReusableCell(DetailMovieHeaderCell.self, for: indexPath) { [weak self] cell in
            guard let self else { return }
            cell.navBarHeight = navigationController?.navigationBar.frame.size.height ?? 0
            cell.configure(with: item)
            cell.playTapped = {
                self.presentYouTubeVC(urlString: item.youtubeURL.absoluteString)
            }
        }
    }
    
    func dequeueUserReviewsCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let userReview = detailMovieSection?.userReviews[indexPath.row] else {
            return tableView.dequeueVoidCell(for: indexPath) ?? UITableViewCell()
        }
        return tableView.setupReusableCell(UserReviewsCell.self, for: indexPath) { cell in
            cell.configure(with: userReview)
        }
    }
}

//MARK: - UITableViewDelegate

extension DetailMovieViewController {
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case SectionType.userReviews.rawValue:
            guard detailMovieSection?.userReviews.isNotEmpty ?? false,
                  let headerView = tableView.dequeueReusableHeaderFooterView(
                    withIdentifier: UserReviewsHeaderView.defaultReuseIdentifier
                  ) as? UserReviewsHeaderView
            else {
                return nil
            }
            return headerView
        default:
            return UIView(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        }
    }
}

//MARK: - UITableViewDataSourcePrefetching

extension DetailMovieViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if detailMovieSection?.userReviews.isNotEmpty ?? false,
           let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last,
           lastVisibleIndexPath.section == 1,
           lastVisibleIndexPath.row >= (detailMovieSection?.userReviews.count ?? 0) - 1 {
            fetchDetailMovies()
        }
    }
}
