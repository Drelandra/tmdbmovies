//
//  GenresMoviesViewController.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import UIKit
import SwiftBaseArchitecture

protocol GenresMoviesViewProtocol: AnyObject {
    func displayGenres(_ movieGenre: GenreMovie)
}

final class GenresMoviesViewController: TitledTableViewController<Genre>, BindableView {

    typealias Presenter = GenresMoviesPresenterProtocol
    var presenter: Presenter?
    
    var genres: [Genre] = [] {
        didSet {
            if genres.isEmpty {
                tableView.setEmptyView(
                    title: .localized(emptyTable: .errorTitle),
                    message: .localized(emptyTable: .errorSubtitle),
                    buttonTitle: .localized(emptyTable: .errorButtonTitle)
                ) { [weak self] in
                    self?.presenter?.fetchGenres()
                }
            } else {
                tableView.removeEmptyView()
            }
            refreshSnapShot()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = .localized(genreMovie: .title)
        presenter?.fetchGenres()
    }
    
    func refreshSnapShot(animated: Bool = true) {
        var snapShot = NSDiffableDataSourceSnapshot<DiffableSectionType, Genre>()
        snapShot.appendSections([.main])
        snapShot.appendItems(genres)
        dataSource.apply(snapShot, animatingDifferences: animated)
    }
}

//MARK: - GenresMoviesViewProtocol

extension GenresMoviesViewController: GenresMoviesViewProtocol {
    func displayGenres(_ movieGenre: GenreMovie) {
        self.genres = movieGenre.genres
    }
}

//MARK: - UITableViewDelegate

extension GenresMoviesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedGenre = genres[safe: indexPath.row] else { return }
        presenter?.routeToDiscoverMovies(selectedGenre)
    }
}
