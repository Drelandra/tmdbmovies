//
//  TitledTableViewController.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 08/02/24.
//

import Foundation
import UIKit

class TitledTableViewController<T: Hashable>: UITableViewController {
    
    lazy var dataSource = UITableViewDiffableDataSource<DiffableSectionType, T>(
        tableView: tableView
    ) { tableView, indexPath, item in
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: TitledCell.defaultReuseIdentifier,
            for: indexPath
        ) as? TitledCell else {
            return tableView.dequeueVoidCell(for: indexPath) ?? UITableViewCell()
        }
        switch item {
        case let item as Genre:
            cell.configure(title: item.name)
        case let item as DiscoverMovie:
            cell.configure(title: item.originalTitle ?? "")
        default: break
        }
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataSource
        tableView.registerEmptyCell()
        tableView.register(TitledCell.self)
        tableView.delegate = self
        
        tableView.setEmptyView(title: "Please wait...", titleOnly: true)
    }
}
