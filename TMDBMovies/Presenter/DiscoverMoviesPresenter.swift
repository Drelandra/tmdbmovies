//
//  DiscoverMoviesPresenter.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture

protocol DiscoverMoviesPresenterProtocol: AnyObject {
    func fetchDiscoverMovies(_ genre: Genre)
    func discoverMoviesFetched(_ model: [DiscoverMovie])
    func routeToDetailMovie(_ movie: DiscoverMovie)
}

final class DiscoverMoviesPresenter: BindablePresenter, DiscoverMoviesPresenterProtocol {
    
    typealias View = DiscoverMoviesViewProtocol
    typealias Interactor = DiscoverMoviesInteractorProtocol
    typealias Router = DiscoverMoviesRouting
    
    weak var view: DiscoverMoviesViewProtocol?
    var interactor: DiscoverMoviesInteractorProtocol?
    var router: Router?
    
    func fetchDiscoverMovies(_ genre: Genre) {
        interactor?.fetchDiscoverMovie(genre)
    }
    
    func discoverMoviesFetched(_ model: [DiscoverMovie]) {
        view?.displayDiscoverMovies(model.unique)
    }
    
    func routeToDetailMovie(_ movie: DiscoverMovie) {
        router?.routeToDetailMovie(movie)
    }
}
