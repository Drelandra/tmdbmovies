//
//  DetailMoviePresenter.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture

protocol DetailMoviePresenterProtocol: AnyObject {
    func loadDetailMovie(_ movie: DiscoverMovie)
    func detailMoviesFetched(_ model: DetailMovieSection)
}

final class DetailMoviePresenter: BindablePresenter, DetailMoviePresenterProtocol {
    
    typealias View = DetailMovieViewProtocol
    typealias Interactor = DetailMovieInteractorProtocol
    typealias Router = Routing
    
    weak var view: DetailMovieViewProtocol?
    var interactor: DetailMovieInteractorProtocol?
    var router: Router?
    
    func loadDetailMovie(_ movie: DiscoverMovie) {
        interactor?.fetchDetailMovie(movie)
    }
    
    func detailMoviesFetched(_ model: DetailMovieSection) {
        view?.displayDetailMovie(model)
    }
}
