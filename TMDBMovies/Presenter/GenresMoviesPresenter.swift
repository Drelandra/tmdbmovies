//
//  GenresMoviesPresenter.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture

protocol GenresMoviesPresenterProtocol: AnyObject {
    func fetchGenres()
    func genresFetched(_ genres: GenreMovie)
    func routeToDiscoverMovies(_ genre: Genre)
}

final class GenresMoviesPresenter: BindablePresenter, GenresMoviesPresenterProtocol {
    
    typealias View = GenresMoviesViewProtocol
    typealias Interactor = GenresMoviesInteractorProtocol
    typealias Router = GenresMoviesRouting
    
    weak var view: GenresMoviesViewProtocol?
    var interactor: GenresMoviesInteractorProtocol?
    var router: Router?
    
    func fetchGenres() {
        interactor?.fetchGenres()
    }
    
    func genresFetched(_ genres: GenreMovie) {
        view?.displayGenres(genres)
    }
    
    func routeToDiscoverMovies(_ genre: Genre) {
        router?.routeToDiscoverMovies(genre)
    }
}
