//
//  GenreMovie.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

struct GenreMovie: Codable {
    let genres: [Genre]
}

struct Genre: Codable, Hashable {
    let id: Int
    let name: String
}
