//
//  AnimalPhotosParam.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

struct DiscoverMovieParam: URLCodableQueriable {
    let page: Int
    let withGenres: String // value: genre id(s) (can be seperated with comma if more than one value)
    
    init(page: Int, withGenres: String) {
        self.page = page
        self.withGenres = withGenres
    }
    
    enum CodingKeys: String, CodingKey {
        case page
        case withGenres = "with_genres"
    }
}
