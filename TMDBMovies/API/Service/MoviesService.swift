//
//  AnimalService.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

protocol MoviesService {
    func fetchGenreMovieList() -> PublishableAPIResult<GenreMovie>
    func fetchDiscoverMovieList(param: DiscoverMovieParam) -> PublishableAPIResult<DiscoverMovieListing>
    func fetchDetailMovie(movieID: Int, page: Int) -> PublishableAPIResult<DetailMovie>
}

final class MoviesAPI: BaseAPI, MoviesService {
    func fetchGenreMovieList() -> PublishableAPIResult<GenreMovie> {
        get(from: "genre/movie/list")
    }
    
    func fetchDiscoverMovieList(param: DiscoverMovieParam) -> PublishableAPIResult<DiscoverMovieListing> {
        get(from: "discover/movie?\(param.asQueryParam)")
    }
    
    func fetchDetailMovie(movieID: Int, page: Int) -> PublishableAPIResult<DetailMovie> {
        get(from: "movie/\(movieID)?append_to_response=videos,reviews&page=\(page)")
    }
}
