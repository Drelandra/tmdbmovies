//
//  DetailMovieInteractor.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture
import Combine

protocol DetailMovieInteractorProtocol: AnyObject {
    func fetchDetailMovie(_ movie: DiscoverMovie)
}

final class DetailMovieInteractor: BindableInteractor, DetailMovieInteractorProtocol {
    
    weak var presenter: DetailMoviePresenterProtocol?
    
    let moviesService: MoviesService = MoviesAPI()
    
    private var eligibleToNextPage: Bool = true
    private var page: Int = 1
    private var totalPage: Int = 1
    private var currentMovie: DiscoverMovie?
    private var cancellables = Set<AnyCancellable>()

    func fetchDetailMovie(_ movie: DiscoverMovie) {
        self.currentMovie = movie
        guard eligibleToNextPage else { return }
        eligibleToNextPage = false
        moviesService.fetchDetailMovie(movieID: movie.id ?? 0, page: page)
        .receive(on: DispatchQueue.main)
        .sink { [weak self] result in
            guard let self else { return }
            switch result {
            case .finished: break
            case .failure(let error):
                debugLog("Error: \(error)")
                handleNextPageCondition(refetch: true)
            }
        } receiveValue: { [weak self] detailMovie in
            guard let self else { return }
            totalPage = detailMovie.reviews.totalPages ?? 0
            handleNextPageCondition()
            presenter?.detailMoviesFetched(detailMovie.asDetailMovieSection)
        }
        .store(in: &cancellables)
    }
}

//MARK: - Helpers

extension DetailMovieInteractor {
    private func handleNextPageCondition(refetch: Bool = false) {
        if page >= totalPage {
            eligibleToNextPage = false
        } else {
            page += 1
            eligibleToNextPage = true
            if refetch, let currentMovie = currentMovie {
                fetchDetailMovie(currentMovie)
            }
        }
    }
}
