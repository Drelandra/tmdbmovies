//
//  DiscoverMoviesInteractor.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture
import Combine

protocol DiscoverMoviesInteractorProtocol: AnyObject {
    func fetchDiscoverMovie(_ genre: Genre)
}

final class DiscoverMoviesInteractor: BindableInteractor, DiscoverMoviesInteractorProtocol {
    
    weak var presenter: DiscoverMoviesPresenterProtocol?
    
    let moviesService: MoviesService = MoviesAPI()
    let dispatchGroup = DispatchGroup()
    
    private var eligibleToNextPage: Bool = true
    private var page: Int = 1
    private var totalPage: Int = 1
    private var currentGenre: Genre?
    private var discoverMovies: [DiscoverMovie] = []
    private var cancellables = Set<AnyCancellable>()
    
    var currentQuery: [Int] = []

    func fetchDiscoverMovie(_ genre: Genre) {
        self.currentGenre = genre
        guard eligibleToNextPage else { return }
        eligibleToNextPage = false
        moviesService.fetchDiscoverMovieList(
            param: DiscoverMovieParam(page: page, withGenres: "\(genre.id)")
        )
        .receive(on: DispatchQueue.main)
        .sink { [weak self] result in
            guard let self else { return }
            switch result {
            case .finished: break
            case .failure(let error):
                debugLog("Error: \(error)")
                handleNextPageCondition(refetch: true)
            }
        } receiveValue: { [weak self] movieDiscoverListing in
            guard let self else { return }
            let discoverMoviesResult = movieDiscoverListing.results
            totalPage = movieDiscoverListing.totalPages
            handleNextPageCondition()
            discoverMovies.append(contentsOf: discoverMoviesResult)
            presenter?.discoverMoviesFetched(discoverMovies)
        }
        .store(in: &cancellables)
    }
}

//MARK: - Helpers

extension DiscoverMoviesInteractor {
    private func handleNextPageCondition(refetch: Bool = false) {
        if page >= totalPage {
            eligibleToNextPage = false
        } else {
            page += 1
            eligibleToNextPage = true
            if refetch, let currentGenre = currentGenre {
                fetchDiscoverMovie(currentGenre)
            }
        }
    }
}
