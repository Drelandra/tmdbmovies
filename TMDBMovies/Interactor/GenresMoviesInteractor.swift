//
//  GenresMoviesInteractor.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import SwiftBaseArchitecture
import Combine

protocol GenresMoviesInteractorProtocol: AnyObject {
    func fetchGenres()
}

final class GenresMoviesInteractor: BindableInteractor, GenresMoviesInteractorProtocol {
    
    weak var presenter: GenresMoviesPresenterProtocol?
    
    let moviesService: MoviesService = MoviesAPI()
    
    private var cancellables = Set<AnyCancellable>()

    func fetchGenres() {
        moviesService.fetchGenreMovieList()
            .receive(on: DispatchQueue.main)
            .sink { result in
                switch result {
                case .finished: break
                case .failure(let error):
                    debugLog("Error: \(error)")
                }
            } receiveValue: { [weak self] movieGenre in
                self?.presenter?.genresFetched(movieGenre)
            }
            .store(in: &cancellables)
    }
}
