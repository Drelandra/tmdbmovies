//
//  DiffableSectionType.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

enum DiffableSectionType {
    case main
    case other
}
