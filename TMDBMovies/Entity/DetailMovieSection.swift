//
//  DetailMovieSection.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

struct DetailMovieSection: Hashable {
    let youtubeURL: URL
    let youtubeThumbnailURL: URL
    let info: String
    let userReviews: [UserReview]
}

struct UserReview: Hashable {
    let username: String
    let review: String
}

extension DetailMovie {
    var asDetailMovieSection: DetailMovieSection {
        let trailerVideoResult = self.videos.results.filter{ $0.type == "Trailer" }.first
        let youtubeTrailerURL: URL = URL(
            string: "https://www.youtube.com/watch?v=\(trailerVideoResult?.key ?? "")"
        ) ?? URL(string: "https://www.youtube.com/watch?v=dQw4w9WgXcQ")!
        let youtubeThumbnailURL = URL(
            string: "https://img.youtube.com/vi/\(trailerVideoResult?.key ?? "")/0.jpg"
        ) ?? URL(string: "https://img.youtube.com/vi/dQw4w9WgXcQ/0.jpg")!
        let userReviews: [UserReview] = self.reviews.results?.compactMap {
            UserReview(username: $0.author ?? "", review: $0.content ?? "")
        } ?? []
        return DetailMovieSection(
            youtubeURL: youtubeTrailerURL,
            youtubeThumbnailURL: youtubeThumbnailURL,
            info: self.overview,
            userReviews: userReviews
        )
    }
}
