//
//  BaseAPI.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import Combine

protocol BaseService {
    var baseURL: String { get }
    var session: URLSession { get }
    var defaultHeader: [String: String] { get }
}

extension BaseService {
    var baseURL: String { "https://api.themoviedb.org/3/" }
    var session: URLSession { URLSession.shared }
    var defaultHeader: [String: String] {
        ["Authorization": Obfuscator().reveal(key: ObfuscatedConstants.obfuscatedTMDBAuth)]
    }
}

protocol BaseAPI: BaseService { }

extension BaseAPI {
    func request<Response: Codable>(endpoint: String) -> PublishableAPIResult<Response> {
        var urlRequest = URLRequest(
            url: URL(string: "\(baseURL)\(endpoint)")!
        )
        urlRequest.allHTTPHeaderFields = defaultHeader
        return session.dataTaskPublisher(
            for: urlRequest
        )
        .tryMap() { element -> Data in
            guard let httpResponse = element.response as? HTTPURLResponse,
                  httpResponse.statusCode == 200 else {
                throw URLError(.badServerResponse)
            }
            printResponse(from: httpResponse, and: element.data)
            return element.data
        }
        .decode(type: Response.self, decoder: JSONDecoder())
        .eraseToAnyPublisher()
    }
    
    func get<Response: Codable>(from endpoint: String) -> PublishableAPIResult<Response> {
        request(endpoint: endpoint)
    }
    
    private func printResponse(from httpResponse: HTTPURLResponse, and data: Data) {
        debugLog(
                 """
                 \nURL Request: \(httpResponse.url?.absoluteString ?? "")
                 Status Code: \(httpResponse.statusCode)
                 Body:\n\(data.prettyString)
                 """
        )
    }
}
