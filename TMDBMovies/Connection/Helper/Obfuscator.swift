//
//  Obfuscator.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

enum ObfuscatedConstants {
    static let obfuscatedTMDBAuth: [UInt8] = [25, 49, 44, 54, 39, 63, 79, 19, 16, 47, 27, 76, 6, 19, 25, 11, 12, 38, 44, 50, 27, 61, 84, 98, 73, 4, 106, 97, 7, 19, 47, 11, 16, 123, 113, 39, 28, 58, 62, 26, 38, 58, 33, 52, 21, 0, 38, 117, 12, 25, 54, 14, 39, 87, 41, 66, 15, 26, 61, 119, 40, 56, 14, 29, 56, 30, 13, 65, 109, 124, 22, 125, 47, 88, 51, 8, 59, 120, 105, 126, 28, 16, 61, 1, 32, 0, 41, 108, 2, 61, 4, 114, 11, 39, 54, 71, 48, 31, 53, 69, 24, 36, 49, 113, 60, 56, 36, 85, 47, 30, 52, 29, 109, 10, 22, 125, 47, 62, 51, 8, 46, 120, 114, 36, 9, 0, 61, 1, 32, 0, 41, 55, 57, 103, 15, 40, 33, 52, 38, 64, 62, 28, 57, 70, 34, 55, 28, 34, 6, 1, 51, 15, 59, 55, 47, 72, 108, 13, 25, 125, 56, 50, 47, 25, 21, 123, 25, 59, 26, 57, 27, 10, 15, 63, 73, 111, 20, 11, 120, 27, 48, 59, 91, 6, 27, 84, 16, 64, 17, 72, 73, 1, 39, 59, 83, 46, 52, 23, 32, 70, 72, 22, 23, 24, 9, 31, 48, 0, 2, 77, 21, 29, 53, 3, 19, 75, 35, 13]
}

/// Referenced from https://gist.github.com/DejanEnspyra/80e259e3c9adf5e46632631b49cd1007
final class Obfuscator {
    
    // MARK: - Variables
    
    /// The salt used to obfuscate and reveal the string.
    private var salt: String = ""
    
    
    // MARK: - Initialization
    
    init() {
        self.salt = [AppDelegate.self, NSObject.self, NSString.self].description
    }
    
    
    // MARK: - Instance Methods
    
    /**
     This method obfuscates the string passed in using the salt
     that was used when the Obfuscator was initialized.
     
     - parameter string: the string to obfuscate
     
     - returns: the obfuscated string in a byte array
     */
    func bytesByObfuscatingString(string: String) -> [UInt8] {
        let text = [UInt8](string.utf8)
        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count
        
        var encrypted = [UInt8]()
        
        for t in text.enumerated() {
            encrypted.append(t.element ^ cipher[t.offset % length])
        }
        
        debugLog("Salt used: \(self.salt)\n")
        debugLog("Swift Code:\n************")
        debugLog("// Original \"\(string)\"")
        debugLog("let key: [UInt8] = \(encrypted)\n")
    
        return encrypted
    }
    
    /**
     This method reveals the original string from the obfuscated
     byte array passed in. The salt must be the same as the one
     used to encrypt it in the first place.
     
     - parameter key: the byte array to reveal
     
     - returns: the original string
     */
    func reveal(key: [UInt8]) -> String {
        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count
        
        var decrypted = [UInt8]()
        
        for k in key.enumerated() {
            decrypted.append(k.element ^ cipher[k.offset % length])
        }
        
        return String(bytes: decrypted, encoding: .utf8)!
    }
}
