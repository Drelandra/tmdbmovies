//
//  BaseAPIHelpers.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import Combine

typealias PublishableAPIResult<Object: Codable> = AnyPublisher<Object, Error>
