//
//  MoviesViewFactory.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit
import SwiftBaseArchitecture

protocol MoviesFactory {
    func makeGenresMoviesViewController() -> UIViewController
    func makeDiscoverMoviesViewController(_ genre: Genre) -> UIViewController
    func makeDetailMovieViewController(_ movie: DiscoverMovie) -> UIViewController
}

final class MoviesViewFactory: MoviesFactory {
    func makeGenresMoviesViewController() -> UIViewController {
        let viewController = GenresMoviesViewController()
        let interactor = GenresMoviesInteractor()
        let presenter = GenresMoviesPresenter()
        let router = GenresMoviesRouter(baseViewController: viewController)
        viewController.bind(with: presenter)
        presenter.bind(with: viewController, interactor: interactor, router: router)
        interactor.bind(with: presenter)
        return viewController
    }
    
    func makeDiscoverMoviesViewController(_ genre: Genre) -> UIViewController {
        let viewController = DiscoverMoviesViewController()
        viewController.selectedGenre = genre
        let interactor = DiscoverMoviesInteractor()
        let presenter = DiscoverMoviesPresenter()
        let router = DiscoverMoviesRouter(baseViewController: viewController)
        viewController.bind(with: presenter)
        presenter.bind(with: viewController, interactor: interactor, router: router)
        interactor.bind(with: presenter)
        return viewController
    }
    
    func makeDetailMovieViewController(_ movie: DiscoverMovie) -> UIViewController {
        let viewController = DetailMovieViewController()
        viewController.selectedMovie = movie
        let interactor = DetailMovieInteractor()
        let presenter = DetailMoviePresenter()
        viewController.bind(with: presenter)
        presenter.bind(with: viewController, interactor: interactor)
        interactor.bind(with: presenter)
        return viewController
    }
}
