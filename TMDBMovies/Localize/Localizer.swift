//
//  Localizer.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 09/02/24.
//

import Foundation

protocol Localizer {
    static var baseName: String { get }
    var key: String { get }
}

extension Localizer where Self: RawRepresentable, Self.RawValue == String {
    var key: String { "\(Self.baseName)_\(rawValue)" }
}

extension String {
    static func localized(_ localizer: Localizer) -> String {
        String(localized: LocalizedStringResource(stringLiteral: localizer.key))
    }
    
    static func localized(genreMovie: Localized.GenreMovie) -> String {
        localized(genreMovie)
    }
    
    static func localized(discoverMovie: Localized.DiscoverMovie) -> String {
        localized(discoverMovie)
    }
    
    static func localized(detailMovie: Localized.DetailMovie) -> String {
        localized(detailMovie)
    }
    
    static func localized(emptyTable: Localized.EmptyTable) -> String {
        localized(emptyTable)
    }
}
