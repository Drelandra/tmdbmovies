//
//  Localized.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 09/02/24.
//

import Foundation

struct Localized {
    enum GenreMovie: String, Localizer {
        static var baseName: String { "genreMovie" }
        case title
    }
    
    enum DiscoverMovie: String, Localizer {
        static var baseName: String { "discoverMovie" }
        case title
    }
    
    enum DetailMovie: String, Localizer {
        static var baseName: String { "detailMovie" }
        case descriptionTitle
        case userReviewsTitle
    }
    
    enum EmptyTable: String, Localizer {
        static var baseName: String { "emptyTable" }
        case loadingTitle
        case errorTitle
        case errorSubtitle
        case errorButtonTitle
    }
}
