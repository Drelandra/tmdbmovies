//
//  UIViewController+Extensions.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 08/02/24.
//

import Foundation
import UIKit
import YouTubePlayerKit

extension UIViewController {
    func presentYouTubeVC(urlString: String) {
        let youTubePlayerViewController = YouTubePlayerViewController(
            player: YouTubePlayer(stringLiteral: urlString)
        )
        youTubePlayerViewController.view.backgroundColor = .black
        present(youTubePlayerViewController, animated: true)
    }
}
