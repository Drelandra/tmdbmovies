//
//  Data+Extensions.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 08/02/24.
//

import Foundation

extension Data {
    var prettyString: String {
        if let jsonObject = try? JSONSerialization.jsonObject(with: self, options: []),
           let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: [.prettyPrinted]),
           let jsonString = String(data: jsonData, encoding: .utf8) {
            return jsonString
        } else {
            return "Failed to parse and print JSON string."
        }
    }
}
