//
//  UITableView&UICollectionView+Extensions.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit

//MARK: - UITableView empty view

extension UITableView {
    func setEmptyView(
        title: String,
        message: String = "",
        buttonTitle: String = "",
        buttonTapped: VoidClosure? = nil,
        titleOnly: Bool = false
    ) {
        let emptyView = UIView(frame: CGRect(x: self.center.x,
                                             y: self.center.y,
                                             width: self.bounds.size.width,
                                             height: self.bounds.size.height))
        
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .label
        titleLabel.font = .systemFont(ofSize: 20, weight: .bold)
        titleLabel.text = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .secondaryLabel
        messageLabel.font = .systemFont(ofSize: 17, weight: .medium)
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        let tipButton = UIButton()
        tipButton.translatesAutoresizingMaskIntoConstraints = false
        tipButton.setTitle(buttonTitle, for: .normal)
        tipButton.tintColor = .white
        tipButton.backgroundColor = .black
        tipButton.layer.cornerRadius = 8
        tipButton.titleLabel?.font = .systemFont(ofSize: 17, weight: .medium)
        tipButton.whenTapped { button in
            buttonTapped?()
        }
        
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        emptyView.addSubview(tipButton)
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: -16),
            titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: emptyView.leadingAnchor, constant:  16),
            titleLabel.trailingAnchor.constraint(equalTo: emptyView.trailingAnchor, constant: -16),
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            messageLabel.leadingAnchor.constraint(equalTo: emptyView.leadingAnchor, constant: 16),
            messageLabel.trailingAnchor.constraint(equalTo: emptyView.trailingAnchor, constant: -16),
            tipButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 12),
            tipButton.centerXAnchor.constraint(equalTo: messageLabel.centerXAnchor),
            tipButton.widthAnchor.constraint(equalToConstant: 150)
        ])
        
        if titleOnly {
            messageLabel.isHidden = true
            tipButton.isHidden = true
        }
        
        self.backgroundView = emptyView
        lastSeparatorStyle = self.separatorStyle
        self.separatorStyle = .none
    }
    
    func removeEmptyView() {
        self.backgroundView = nil
        self.separatorStyle = lastSeparatorStyle
    }
    
    private static var separatorStyleAssociatedKey: UInt8 = 0
    var lastSeparatorStyle: UITableViewCell.SeparatorStyle {
        get {
            guard let storedSeparatorStyle = objc_getAssociatedObject(
                self, &UITableView.separatorStyleAssociatedKey
            ) as? UITableViewCell.SeparatorStyle else {
                objc_setAssociatedObject(
                    self,
                    &UITableView.separatorStyleAssociatedKey,
                    UITableViewCell.SeparatorStyle.singleLine, .OBJC_ASSOCIATION_RETAIN
                )
                return .singleLine
            }
            return storedSeparatorStyle
        }
        set {
            objc_setAssociatedObject(
                self,
                &UITableView.separatorStyleAssociatedKey,
                newValue,
                .OBJC_ASSOCIATION_RETAIN
            )
        }
    }
}

//MARK: - UITable & UICollection Identifiers

extension UITableView {
    func register<Cell: UITableViewCell>(_ type: Cell.Type) {
        register(Cell.self, forCellReuseIdentifier: Cell.defaultReuseIdentifier)
    }
    
    func registerHeaderFooter<View: UITableViewHeaderFooterView>(_ type: View.Type) {
        register(View.self, forHeaderFooterViewReuseIdentifier: View.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<Cell: UITableViewCell>(_ type: Cell.Type, for indexPath: IndexPath) -> Cell? {
        dequeueReusableCell(withIdentifier: Cell.defaultReuseIdentifier, for: indexPath) as? Cell
    }
    
    func registerEmptyCell() {
        register(VoidTableCell.self)
    }
    
    func dequeueVoidCell(for indexPath: IndexPath) -> VoidTableCell? {
        dequeueReusableCell(VoidTableCell.self, for: indexPath)
    }
    
    func setupReusableCell<Cell: UITableViewCell>(
        _ type: Cell.Type,
        for indexPath: IndexPath,
        setup: (Cell) -> Void
    ) -> UITableViewCell {
        guard let cell = dequeueReusableCell(
            withIdentifier: Cell.defaultReuseIdentifier,
            for: indexPath) as? Cell else {
            return .init()
        }
        setup(cell)
        return cell
    }
}

extension UICollectionView {
    func register<Cell: UICollectionViewCell>(_ type: Cell.Type) {
        register(Cell.self, forCellWithReuseIdentifier: Cell.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<Cell: UICollectionViewCell>(_ type: Cell.Type, for indexPath: IndexPath) -> Cell? {
        dequeueReusableCell(withReuseIdentifier: Cell.defaultReuseIdentifier, for: indexPath) as? Cell
    }
    
    func registerEmptyCell() {
        register(VoidCollectionCell.self)
    }
    
    func dequeueVoidCell(for indexPath: IndexPath) -> VoidCollectionCell? {
        dequeueReusableCell(VoidCollectionCell.self, for: indexPath)
    }
    
    func setupReusableCell<Cell: UICollectionViewCell>(
        _ type: Cell.Type,
        for indexPath: IndexPath,
        setup: (Cell) -> Void
    ) -> UICollectionViewCell {
        guard let cell = dequeueReusableCell(
            withReuseIdentifier: Cell.defaultReuseIdentifier,
            for: indexPath) as? Cell else {
            return .init()
        }
        setup(cell)
        return cell
    }
}

extension UITableViewCell {
    static var defaultReuseIdentifier: String { String(describing: Self.self) }
}

extension UICollectionViewCell {
    static var defaultReuseIdentifier: String { String(describing: Self.self) }
}

extension UITableViewHeaderFooterView {
    static var defaultReuseIdentifier: String { String(describing: Self.self) }
}

//MARK: - Void Cells

final class VoidTableCell: UITableViewCell {
    
    var height: CGFloat = .zero
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .clear
    }
    
}

final class VoidCollectionCell: UICollectionViewCell {
    
    var size: CGSize = UICollectionViewFlowLayout.automaticSize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .clear
    }
}
