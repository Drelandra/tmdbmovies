//
//  UIControl+Extensions.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation
import UIKit

enum UIControlAssociatedKeys {
    static var DefaultAssociatedKey: UInt8 = 0
    static var TouchDownAssociatedKey: UInt8 = 1
    static var TouchDownRepeatAssociatedKey: UInt8 = 2
    static var TouchDragInsideAssociatedKey: UInt8 = 3
    static var TouchDragOutsideAssociatedKey: UInt8 = 4
    static var TouchDragEnterAssociatedKey: UInt8 = 5
    static var TouchDragExitAssociatedKey: UInt8 = 6
    static var TouchUpInsideAssociatedKey: UInt8 = 7
    static var TouchUpOutsideAssociatedKey: UInt8 = 8
    static var TouchCancelAssociatedKey: UInt8 = 9
    static var ValueChangedAssociatedKey: UInt8 = 10
    static var PrimaryActionTriggeredAssociatedKey: UInt8 = 11
    static var MenuActionTriggeredAssociatedKey: UInt8 = 12
    static var EditingDidBeginAssociatedKey: UInt8 = 13
    static var EditingChangedAssociatedKey: UInt8 = 14
    static var EditingDidEndAssociatedKey: UInt8 = 15
    static var EditingDidEndOnExitAssociatedKey: UInt8 = 16
    static var AllTouchEventsAssociatedKey: UInt8 = 17
    static var AllEditingEventsAssociatedKey: UInt8 = 18
    static var ApplicationReservedAssociatedKey: UInt8 = 19
    static var SystemReservedAssociatedKey: UInt8 = 20
    static var AllEventsAssociatedKey: UInt8 = 21
    
    static var associatedKeys: [UIControl.Event: UnsafeRawPointer] = [
        .touchDown: .init(&TouchDownAssociatedKey),
        .touchDownRepeat: .init(&TouchDownRepeatAssociatedKey),
        .touchDragInside: .init(&TouchDragInsideAssociatedKey),
        .touchDragOutside: .init(&TouchDragOutsideAssociatedKey),
        .touchDragEnter: .init(&TouchDragEnterAssociatedKey),
        .touchUpInside: .init(&TouchUpInsideAssociatedKey),
        .touchUpOutside: .init(&TouchUpOutsideAssociatedKey),
        .touchCancel: .init(&TouchCancelAssociatedKey),
        .valueChanged: .init(&ValueChangedAssociatedKey),
        .primaryActionTriggered: .init(&PrimaryActionTriggeredAssociatedKey),
        .menuActionTriggered: .init(&MenuActionTriggeredAssociatedKey),
        .editingDidBegin: .init(&EditingDidBeginAssociatedKey),
        .editingChanged: .init(&EditingChangedAssociatedKey),
        .editingDidEnd: .init(&EditingDidEndAssociatedKey),
        .editingDidEndOnExit: .init(&EditingDidEndOnExitAssociatedKey),
        .allTouchEvents: .init(&AllTouchEventsAssociatedKey),
        .allEditingEvents: .init(&AllEditingEventsAssociatedKey),
        .applicationReserved: .init(&ApplicationReservedAssociatedKey),
        .systemReserved: .init(&SystemReservedAssociatedKey),
        .allEvents: .init(&AllEventsAssociatedKey),
    ]
}

extension UIControl.Event: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(rawValue)
    }
}

@objc final class ActionClosure: NSObject {
    typealias Action = Closure<Any>
    let action: Action
    
    init(_ action: @escaping Action) {
        self.action = action
    }
    
    @objc func invoke(by sender: Any) {
        action(sender)
    }
}

extension UIControl {
    func whenTapped(run closure: @escaping Closure<Any>) {
        whenTriggered(by: .touchUpInside, run: closure)
    }
    
    func whenTriggered(by event: UIControl.Event, run closure: @escaping Closure<Any>) {
        let action = ActionClosure(closure)
        addTarget(action, action: #selector(ActionClosure.invoke(by:)), for: event)
        objc_setAssociatedObject(
            self,
            UIControlAssociatedKeys.associatedKeys[event] ?? UnsafeRawPointer(&UIControlAssociatedKeys.DefaultAssociatedKey),
            action,
            .OBJC_ASSOCIATION_RETAIN
        )
    }
}
