//
//  ClosureHelper.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

typealias ReturnClosure<Return> = () -> Return
typealias ReturnSingleArgClosure<Argument, Return> = (Argument) -> Return

typealias VoidClosure = ReturnClosure<Void>
typealias Closure<Argument> = ReturnSingleArgClosure<Argument, Void>

func methodOf<Object: AnyObject, Argument>(
    _ object: Object,
    _ method: @escaping (Object) -> Closure<Argument>
) -> Closure<Argument> {
    { [weak object] arg in
        guard let object = object else { return }
        method(object)(arg)
    }
}
