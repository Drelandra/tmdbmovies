//
//  Logger.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 08/02/24.
//

import Foundation

func debugLog(
    fileID: String = #fileID,
    function: String = #function,
    line: Int = #line,
    _ text: String
) {
    #if DEBUG
    print("\(fileID) \(function) at line \(line) | \(text)")
    #endif
}


func debugLog(
    fileID: String = #fileID,
    function: String = #function,
    line: Int = #line,
    _ texts: String...
) {
    #if DEBUG
    print("\(fileID) \(function) at line \(line) | \(texts)")
    #endif
}
