//
//  URLQueriable.swift
//  TMDBMovies
//
//  Created by Andre Elandra on 06/02/24.
//

import Foundation

//MARK: - URLQueriable

typealias URLCodableQueriable = URLQueriable & Encodable

protocol URLQueriable {
    var asQueryParam: String { get }
}

extension URLQueriable where Self: Encodable {
    var asQueryParam: String {
        guard let data = try? JSONEncoder().encode(self),
              let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap({ $0 as? [String: Any] })
        else { return "" }
        return dictionary.reduce("") { previousQuery, pair in
            guard let compatible = pair.value as? URLQueryCompatible,
                  let value = compatible.asQueryString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
                  value.isEmpty == false else {
                return previousQuery
            }
            let query = "\(pair.key)=\(value)"
            return previousQuery.isEmpty ? query : "\(previousQuery)&\(query)"
        }
    }
}

//MARK: - URLQueryCompatible

protocol URLQueryCompatible {
    var asQueryString: String { get }
}

extension String: URLQueryCompatible {
    var asQueryString: String {
        self
    }
}

extension NSString: URLQueryCompatible {
    var asQueryString: String {
        self as String
    }
}

extension Int: URLQueryCompatible {
    var asQueryString: String {
        "\(self)"
    }
}

extension NSNumber: URLQueryCompatible {
    var asQueryString: String {
        self.stringValue
    }
}

extension Int64: URLQueryCompatible {
    var asQueryString: String {
        "\(self)"
    }
}

extension Double: URLQueryCompatible {
    var asQueryString: String {
        String(format: "%.2f", self)
    }
}

extension Float: URLQueryCompatible {
    var asQueryString: String {
        String(format: "%.2f", self)
    }
}

extension Bool: URLQueryCompatible {
    var asQueryString: String {
        self ? "true" : "false"
    }
}

extension Array: URLQueryCompatible {
    var asQueryString: String {
        reduce("") { previous, element in
            guard let compatible = element as? URLQueryCompatible else { return previous }
            return previous.isEmpty ? compatible.asQueryString : "\(previous),\(compatible.asQueryString)"
        }
    }
}

extension NSArray: URLQueryCompatible {
    var asQueryString: String {
        reduce("") { previous, element in
            guard let compatible = element as? URLQueryCompatible else { return previous }
            return previous.isEmpty ? compatible.asQueryString : "\(previous),\(compatible.asQueryString)"
        }
    }
}

extension URLQueryCompatible where Self: RawRepresentable, RawValue: URLQueryCompatible {
    var asQueryString: String {
        rawValue.asQueryString
    }
}
